import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from '../../services/post.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { CommunicationService } from '../../services/communication.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent implements OnInit {

  public loading: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public postService: PostService,
    public snackBarService: SnackBarService,
    private communicationService: CommunicationService
  ) { }

  ngOnInit(): void {
  }

  delete() {
    this.loading = true;
    this.postService.deletePost(this.data.id)
      .subscribe((response) => {
        this.loading = false;
        this.dialogRef.close();
        this.communicationService.transferData(+this.data.id);
      },
        (error) => {
          this.loading = false;
          this.snackBarService.showErrorMessage(error)
        }
      );
  }

}
