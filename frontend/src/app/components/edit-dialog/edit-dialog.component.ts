import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ImgurService } from '../../services/imgur.service';
import { PostService } from '../../services/post.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { EditPost } from '../../models/post/edit-post';
import { CommunicationService } from '../../services/communication.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css']
})
export class EditDialogComponent implements OnInit {

  private id: number;
  public body: string;
  public image: string;

  public radioButtonPhoto: string;
  public get showSelectNewPhoto(): boolean {
    return this.radioButtonPhoto == "new";
  }

  public imageUrl: string;
  public imageFile: File;

  public loading: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private postService: PostService,
    private snackBarService: SnackBarService,
    private imgurService: ImgurService,
    private communicationService: CommunicationService
  ) { }

  ngOnInit(): void {
    this.id = this.data.id;
    this.body = this.data.body;
    this.image = this.data.image != "" ? this.data.image : null;
  }

  public editPost() {
    if (this.radioButtonPhoto != null && this.radioButtonPhoto == "new" && this.imageFile == undefined) {
      this.snackBarService.showErrorMessage("No image is selected");
      return;
    }

    this.loading = true;

    if (this.radioButtonPhoto != null && this.radioButtonPhoto == "remove") {
      this.image = null;
    }

    let editPost: EditPost = {
      postId: this.id,
      body: this.body,
      previewImage: this.image
    };

    if (this.imageFile && this.radioButtonPhoto != null && this.radioButtonPhoto == "new") {
      this.imgurService.uploadToImgur(this.imageFile, 'title').subscribe(
        data => {
          editPost.previewImage = data.body.data.link;
          this.postService.editPost(editPost).subscribe(
            (response) => {
              this.loading = false;
              this.dialogRef.close();
              this.communicationService.updateData(editPost);
            },
            (error) => {
              this.loading = false;
              this.snackBarService.showErrorMessage(error);
            }
          );
        },
        (error) => {
          this.loading = false;
          this.snackBarService.showErrorMessage(error);
        }
      )
    }
    else {
      this.postService.editPost(editPost).subscribe(
        (response) => {
          this.loading = false;
          this.dialogRef.close();
          this.communicationService.updateData(editPost);
        },
        (error) => {
          this.loading = false;
          this.snackBarService.showErrorMessage(error);
        }
      )
    }
  }

  public loadImage(target: any) {
    this.imageFile = target.files[0];

    if (!this.imageFile) {
      target.value = '';
      return;
    }

    if (this.imageFile.size / 1000000 > 5) {
      target.value = '';
      this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
      return;
    }

    const reader = new FileReader();
    reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
    reader.readAsDataURL(this.imageFile);
  }

  public removeImage() {
    this.imageUrl = undefined;
    this.imageFile = undefined;
  }

}
