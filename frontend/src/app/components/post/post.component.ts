import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { LikeService } from '../../services/like.service';
import { CommentService } from '../../services/comment.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { DeleteDialogService } from '../../services/delete-dialog.service';
import { EditDialogService } from '../../services/edit-dialog.service';
import { empty, Observable, Subject, Subscription } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { NewComment } from '../../models/comment/new-comment';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { CommunicationService } from '../../services/communication.service';
import { SharePostDialogService } from '../../services/share-post-dialog.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit, OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showComments = false;
    public newComment = {} as NewComment;

    private unsubscribe$ = new Subject<void>();

    public likes: number = 0;
    public disLikes: number = 0;

    public usersWhoLiked: string[] = [];
    public usersWhoDisliked: string[] = [];

    public get numOfUsersWhoLiked() {
        return this.post.reactions.filter(e => e.isLike === true).length;
    }
    public get numOfUsersWhoDisliked() {
        return this.post.reactions.filter(e => e.isLike === false).length;
    }

    private removeCommentSubscription: Subscription

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private deleteDialogService: DeleteDialogService,
        private editDialogService: EditDialogService,
        private communicationService: CommunicationService,
        private sharePostDialogService: SharePostDialogService
    ) { }

    ngOnInit() {
        this.getReactions();
        this.getUsersWhoReacted();

        this.removeCommentSubscription = this.communicationService.deleteCommentchannel$.subscribe(
            (data) => {
                this.post.comments = this.post.comments.filter(e => e.id !== data)
            }
        )
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.removeCommentSubscription.unsubscribe();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public editPost() {
        this.editDialogService.openEditPostDialog(this.post.id, this.post.body, this.post.previewImage);
    }

    public deletePost() {
        this.deleteDialogService.openDeleteDialog(this.post.id);
    }

    public likePost(liked: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, liked)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, liked)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;
                this.getReactions();
                this.getUsersWhoReacted();
            });
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public sharePost() {
        this.sharePostDialogService.openEmailSendDialoh(this.post.id, this.currentUser.id);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    private getReactions() {
        this.likes = this.post.reactions.filter(e => e.isLike === true).length;
        this.disLikes = this.post.reactions.filter(e => e.isLike === false).length;
    }

    private getUsersWhoReacted() {
        this.usersWhoLiked = [];
        this.usersWhoDisliked = [];

        if (this.numOfUsersWhoLiked > 10) {
            for (let i = 0; i < 9; i++) {
                let currentReaction = this.post.reactions[i];
                if (currentReaction.isLike === true) {
                    this.usersWhoLiked.push(this.post.reactions[i].user.userName);
                }
            }
            this.usersWhoLiked.push(`And ${this.numOfUsersWhoLiked - 9} more ...`);
        }
        else {
            this.post.reactions.forEach(e => {
                if (e.isLike === true) {
                    this.usersWhoLiked.push(e.user.userName);
                }
            });
        }

        if (this.numOfUsersWhoDisliked > 10) {
            for (let i = 0; i < 9; i++) {
                let currentReaction = this.post.reactions[i];
                if (currentReaction.isLike === false) {
                    this.usersWhoDisliked.push(this.post.reactions[i].user.userName);
                }
            }
            this.usersWhoDisliked.push(`And ${this.numOfUsersWhoDisliked - 9} more ...`);
        }
        else {
            this.post.reactions.forEach(e => {
                if (e.isLike === false) {
                    this.usersWhoDisliked.push(e.user.userName);
                }
            });
        }
    }
}
