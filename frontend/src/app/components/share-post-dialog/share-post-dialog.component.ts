import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { PostService } from '../../services/post.service';
import { SharedPost } from '../../models/post/shared-post';
import { SnackBarService } from '../../services/snack-bar.service';

@Component({
  selector: 'app-share-post-dialog',
  templateUrl: './share-post-dialog.component.html',
  styleUrls: ['./share-post-dialog.component.sass']
})
export class SharePostDialogComponent implements OnInit {

  public emailControl: FormControl = new FormControl("", [
    Validators.required, Validators.email
  ]);
  public loading: boolean = false;

  private postId: number;
  private userId: number;

  constructor(
    public dialogRef: MatDialogRef<SharePostDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private postService: PostService,
    private snackBarService: SnackBarService
  ) { }

  ngOnInit(): void {
    this.postId = this.data.postId;
    this.userId = this.data.userId;
  }

  public share() {
    let post: SharedPost = {
      postId: this.postId,
      userId: this.userId,
      email: this.emailControl.value
    };
    this.loading = true;
    this.postService.sharePost(post).subscribe(
      (data) => {
        this.loading = false;
        this.dialogRef.close();
      },
      (error) => {
        this.loading = false;
        (error) => this.snackBarService.showErrorMessage(error);
      }
    );
  }

  public GetErrorMEssage() {
    if (this.emailControl.hasError('email')) {
      return "Please enter a valid email address";
    }

    if (this.emailControl.hasError('required')) {
      return "Email is required";
    }
  }

}
