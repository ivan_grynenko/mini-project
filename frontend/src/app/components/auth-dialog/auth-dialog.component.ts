import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogType } from '../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
    templateUrl: './auth-dialog.component.html',
    styleUrls: ['./auth-dialog.component.sass']
})
export class AuthDialogComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public userName: string;
    public password: string;
    public avatar: string;
    public email: string;

    public hidePass = true;
    public title: string;
    private unsubscribe$ = new Subject<void>();

    public emailControl: FormControl = new FormControl("", [
        Validators.required, Validators.email
    ]);
    public nameControl: FormControl = new FormControl("", [
        Validators.required, Validators.minLength(3), Validators.maxLength(50)
    ]);
    public passwordControl: FormControl = new FormControl("", [
        Validators.required
    ]);

    constructor(
        private dialogRef: MatDialogRef<AuthDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private authService: AuthenticationService,
        private snackBarService: SnackBarService
    ) { }

    public ngOnInit() {
        this.avatar = 'https://avatars.mds.yandex.net/get-ott/374297/2a000001616b87458162c9216ccd5144e94d/orig';
        this.title = this.data.dialogType === DialogType.SignIn ? 'Sign In' : 'Sign Up';
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public signIn() {
        this.authService
            .login({ email: this.email, password: this.password })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response), (error) => this.snackBarService.showErrorMessage(error));
    }

    public signUp() {
        this.authService
            .register({ userName: this.userName, password: this.password, email: this.email, avatar: this.avatar })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response), (error) => this.snackBarService.showErrorMessage(error));
    }

    public getEmailErrorMessage() {
        if (this.emailControl.hasError("required")) {
            return "Email field is required";
        }

        if (this.emailControl.hasError("email")) {
            return "Invalid email";
        }
    }

    public getPasswordErrorMessage() {
        if (this.passwordControl.invalid) {
            return "Password field is required";
        }
    }

    public getUserNameErrorMessage() {
        if (this.nameControl.hasError("required")) {
            return "Username is a required field";
        }

        if (this.nameControl.hasError("minlength")) {
            return "It has to have at least 3 chars";
        }

        if (this.nameControl.hasError("maxlength")) {
            return "But not more than 50";
        }
    }

    public isFromInvalid() {
        if (this.data.dialogType === DialogType.SignIn) {
            return (this.emailControl.invalid || this.passwordControl.invalid) ? true : false;
        }
        else {
            return (this.emailControl.invalid || this.passwordControl.invalid || this.nameControl.invalid) ? true : false;
        }
    }
}
