import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { CommentService } from '../../services/comment.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { CommunicationService } from '../../services/communication.service';

@Component({
  selector: 'app-delete-comment-dialog',
  templateUrl: './delete-comment-dialog.component.html',
  styleUrls: ['./delete-comment-dialog.component.sass']
})
export class DeleteCommentDialogComponent implements OnInit {

  private commentId: number;

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commentService: CommentService,
    private snackBarService: SnackBarService,
    private communicationService: CommunicationService,
  ) { }

  ngOnInit(): void {
    this.commentId = this.data.id;
  }

  public deleteComment() {
    this.commentService.deleteComment(this.commentId).subscribe(
      (resp) => {
        this.dialogRef.close();
        this.communicationService.updateComments(this.commentId);
      },
      (error) => {
        (error) => this.snackBarService.showErrorMessage(error);
      }
    );
  }
}