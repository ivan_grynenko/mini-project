import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject, Subscription } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { PostService } from '../../services/post.service';
import { EventService } from '../../services/event.service';
import { ImgurService } from '../../services/imgur.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { CommunicationService } from '../../services/communication.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { ToastrService } from 'ngx-toastr';
import { EditPost } from '../../models/post/edit-post';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;
    public isOnlyLiked: boolean = false;

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    public postHub: HubConnection;
    public commentHub: HubConnection;

    private unsubscribe$ = new Subject<void>();
    private removeSubscription: Subscription;
    private updateSubscription: Subscription;

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private toastr: ToastrService,
        private communicationService: CommunicationService,
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
        this.commentHub.stop();
        this.removeSubscription.unsubscribe();
        this.updateSubscription.unsubscribe();
    }

    public ngOnInit() {
        this.registerHub();
        this.getPosts();
        this.getUser();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });

        this.removeSubscription = this.communicationService.removeChannel$.subscribe(
            data => {
                this.posts = this.posts.filter(e => e.id !== data);
                this.cachedPosts = this.cachedPosts.filter(e => e.id !== data);
            }
        );
        this.updateSubscription = this.communicationService.updatehannel$.subscribe(
            data => {
                let post = this.posts.find(e => e.id == data.postId);
                post.previewImage = data.previewImage;
                post.body = data.body;
            }
        );
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
                this.showToastrMessage("Post was added");
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public sliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyMine = true;
            this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id);
        } else {
            this.isOnlyMine = false;
            this.posts = this.cachedPosts;
        }
    }

    public showOnlyLikedPosts(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyLiked = true;
            let tempPosts = [];
            for (let post of this.posts) {
                post.reactions.forEach(e => {
                    if (e.user.id === this.currentUser.id && e.isLike === true) {
                        tempPosts.push(post);
                    }
                });
            }
            this.posts = tempPosts;
        }
        else {
            this.isOnlyLiked = false;
            this.posts = this.cachedPosts;
        }
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewPost', (newPost: Post) => {
            console.log("from back - signal");
            if (newPost) {
                this.addNewPost(newPost);
            }
        });
        this.postHub.on('EditPost', (editPost: EditPost) => this.showToastrMessage("Post was updated"));
        this.postHub.on('DeletePost', () => this.showToastrMessage("Post was removed"));
        this.postHub.on('PostShare', () => this.showToastrMessage("Post was shared"));

        this.commentHub = new HubConnectionBuilder()
            .withUrl("https://localhost:44344/notifications/comments")
            .build();
        this.commentHub.start()
            .catch((error) => this.snackBarService.showErrorMessage(error));

        this.commentHub.on('NewComment', () => this.showToastrMessage("Comment was added"));
        this.commentHub.on('EditComment', () => this.showToastrMessage("Comment was updated"));
        this.commentHub.on('DeleteComment', () => this.showToastrMessage("Comment was removed"));
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            if (this.isOnlyLiked) {
                this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
                return;
            }
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }
        }
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    private showToastrMessage(message: string) {
        this.toastr.success(message, null, {
            timeOut: 7000,
            extendedTimeOut: 2000
        });
    }
}
