import { Component, Input, ViewChild, ElementRef, ChangeDetectorRef, OnInit } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';
import { CommentService } from '../../services/comment.service';
import { EditComment } from 'src/app/models/comment/edit-comment';
import { DeleteCommentDialogService } from '../../services/delete-comment-dialog.service';
import { Observable, empty, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { DialogType } from '../../models/common/auth-dialog-type';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { AuthenticationService } from '../../services/auth.service';
import { LikeService } from '../../services/like.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {

    @Input() public comment: Comment;
    @Input() public currentUser: User;

    @ViewChild('editingElement', { static: false })
    inputComment: ElementRef;

    public editingMode: boolean = false;
    public loading: boolean = false;

    public likes: number = 0;
    public disLikes: number = 0;

    private unsubscribe$ = new Subject<void>();

    public usersWhoLiked: string[] = [];
    public usersWhoDisliked: string[] = [];

    public get numOfUsersWhoLiked() {
        return this.comment.reactions.filter(e => e.isLike === true).length;
    }
    public get numOfUsersWhoDisliked() {
        return this.comment.reactions.filter(e => e.isLike === false).length;
    }

    constructor(
        private changeDetector: ChangeDetectorRef,
        private commentService: CommentService,
        private deleteCommentDialogService: DeleteCommentDialogService,
        private authDialogService: AuthDialogService,
        private authService: AuthenticationService,
        private likeService: LikeService
    ) { }

    ngOnInit() {
        this.getReactions();
        this.getUsersWhoReacted();;
    }

    public editComment() {
        this.editingMode = true;
        this.changeDetector.detectChanges();
        this.inputComment.nativeElement.focus();
    }

    public deleteComment() {
        this.deleteCommentDialogService.openDeleteCommentDialog(this.comment.id);
    }

    public onEditingDone() {
        this.loading = true;
        const updComment: EditComment = {
            commentId: this.comment.id,
            body: this.inputComment.nativeElement.value
        }

        this.commentService.editComment(updComment).subscribe(
            (resp) => {
                this.comment.body = updComment.body;
                this.loading = false;
                this.editingMode = false;
            },
            (error) => {
                this.loading = false;
                ///....
            }
        );
    }

    public likeComment(liked: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp, liked)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser, liked)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => {
                this.comment = comment;
                this.getReactions();
                this.getUsersWhoReacted();
            });
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private getReactions() {
        this.likes = this.comment.reactions.filter(e => e.isLike === true).length;
        this.disLikes = this.comment.reactions.filter(e => e.isLike === false).length;
    }

    private getUsersWhoReacted() {
        this.usersWhoLiked = [];
        this.usersWhoDisliked = [];

        if (this.numOfUsersWhoLiked > 10) {
            for (let i = 0; i < 9; i++) {
                let currentReaction = this.comment.reactions[i];
                if (currentReaction.isLike === true) {
                    this.usersWhoLiked.push(this.comment.reactions[i].user.userName);
                }
            }
            this.usersWhoLiked.push(`And ${this.numOfUsersWhoLiked - 9} more ...`);
        }
        else {
            this.comment.reactions.forEach(e => {
                if (e.isLike === true) {
                    this.usersWhoLiked.push(e.user.userName);
                }
            });
        }

        if (this.numOfUsersWhoDisliked > 10) {
            for (let i = 0; i < 9; i++) {
                let currentReaction = this.comment.reactions[i];
                if (currentReaction.isLike === false) {
                    this.usersWhoDisliked.push(this.comment.reactions[i].user.userName);
                }
            }
            this.usersWhoDisliked.push(`And ${this.numOfUsersWhoDisliked - 9} more ...`);
        }
        else {
            this.comment.reactions.forEach(e => {
                if (e.isLike === false) {
                    this.usersWhoDisliked.push(e.user.userName);
                }
            });
        }
    }
}
