import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { EditComment } from '../models/comment/edit-comment';
import { NewReaction } from '../models/reactions/newReaction';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public editComment(comment: EditComment) {
        return this.httpService.editFullRequest<EditComment>(`${this.routePrefix}`, comment);
    }

    public deleteComment(id: number) {
        return this.httpService.deleteFullRequest<any>(`${this.routePrefix}/${id.toString()}`);
    }

    public likeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<NewReaction>(`${this.routePrefix}/like`, reaction);
    }
}
