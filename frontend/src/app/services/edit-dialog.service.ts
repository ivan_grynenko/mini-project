import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditDialogComponent } from '../components/edit-dialog/edit-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class EditDialogService {

  constructor(
    public dialog: MatDialog
  ) { }

  public openEditPostDialog(id: number, body: string, image: string) {
    this.dialog.open(EditDialogComponent, {
      data: {
        id: id,
        body: body,
        image: image
      },
      minWidth: 400,
      maxWidth: 800,
      minHeight: 200,
      maxHeight: 700,
      autoFocus: true
    })
  }

}
