import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SharePostDialogComponent } from '../components/share-post-dialog/share-post-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class SharePostDialogService {

  constructor(
    private matDialog: MatDialog
  ) { }

  public openEmailSendDialoh(postId: number, userId: number) {
    this.matDialog.open(SharePostDialogComponent, {
      data: {
        postId: postId,
        userId: userId
      },
      maxHeight: 400,
      minWidth: 300
    });
  }
}
