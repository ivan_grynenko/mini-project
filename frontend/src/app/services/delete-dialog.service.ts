import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from '../components/delete-dialog/delete-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DeleteDialogService {

  constructor(
    public dialog: MatDialog
  ) { }

  public openDeleteDialog(id: number) {
    const dialog = this.dialog.open(DeleteDialogComponent, {
      data: { id: id },
      minWidth: 100,
      minHeight: 50,
      autoFocus: true
    });
  }
}
