import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { CommentService } from './comment.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(
        private authService: AuthenticationService,
        private postService: PostService,
        private commentService: CommentService
    ) { }

    public likePost(post: Post, currentUser: User, liked: boolean) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: liked,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike === liked);
        let temp: Reaction[];

        if (hasReaction) {
            temp = innerPost.reactions.splice(innerPost.reactions.findIndex(e => e.isLike === liked && e.user.id === currentUser.id), 1);
        }
        else {
            temp = [{ isLike: liked, user: currentUser }];
            innerPost.reactions = innerPost.reactions.concat(temp);
        }

        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike === liked);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                hasReaction
                    ? innerPost.reactions.splice(innerPost.reactions.findIndex(e => e.isLike === temp[0].isLike && e.user.id === temp[0].user.id), 1)
                    : innerPost.reactions = innerPost.reactions.concat(temp);

                return of(innerPost);
            })
        );
    }

    public likeComment(comment: Comment, currentUser: User, liked: boolean) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: liked,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id && x.isLike === liked);
        let temp: Reaction[];

        if (hasReaction) {
            temp = innerComment.reactions.splice(innerComment.reactions.findIndex(e => e.isLike === liked && e.user.id === currentUser.id), 1);
        }
        else {
            temp = [{ isLike: liked, user: currentUser }];
            innerComment.reactions = innerComment.reactions.concat(temp);
        }

        hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id && x.isLike === liked);

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                hasReaction
                    ? innerComment.reactions.splice(innerComment.reactions.findIndex(e => e.isLike === temp[0].isLike && e.user.id === temp[0].user.id), 1)
                    : innerComment.reactions = innerComment.reactions.concat(temp);

                return of(innerComment);
            })
        );
    }
}