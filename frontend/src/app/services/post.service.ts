import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { EditPost } from '../models/post/edit-post';
import { SharedPost } from '../models/post/shared-post';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(
        private httpService: HttpInternalService,
        private http: HttpClient
    ) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public editPost(editPost: EditPost) {
        return this.httpService.editFullRequest<EditPost>(`${this.routePrefix}`, editPost);
    }

    public deletePost(id: number) {
        return this.httpService.deleteFullRequest<Post>(`${this.routePrefix}/${id.toString()}`);
    }

    public sharePost(post: SharedPost) {
        return this.httpService.postRequest<SharedPost>(`${this.routePrefix}/share`, post);
    }
}