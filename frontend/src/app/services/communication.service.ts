import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { EditPost } from '../models/post/edit-post';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private removeChannel = new Subject<number>();
  public removeChannel$ = this.removeChannel.asObservable();

  private updatehannel = new Subject<EditPost>();
  public updatehannel$ = this.updatehannel.asObservable();

  private deleteCommentchannel = new Subject<number>();
  public deleteCommentchannel$ = this.deleteCommentchannel.asObservable();

  constructor() { }

  public transferData(data: number) {
    this.removeChannel.next(data);
  }

  public updateData(post: EditPost) {
    this.updatehannel.next(post);
  }

  public updateComments(data: number) {
    this.deleteCommentchannel.next(data);
  }
}
