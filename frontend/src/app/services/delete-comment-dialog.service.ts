import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeleteCommentDialogComponent } from '../components/delete-comment-dialog/delete-comment-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DeleteCommentDialogService {

  constructor(
    public matDialog: MatDialog
  ) { }

  public openDeleteCommentDialog(id: number) {
    this.matDialog.open(DeleteCommentDialogComponent, {
      data: { id: id },
      minWidth: 100,
      minHeight: 50,
      autoFocus: true
    })
  }
}
