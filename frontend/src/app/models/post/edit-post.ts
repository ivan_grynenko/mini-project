export interface EditPost {
    postId: number;
    body: string;
    previewImage: string;
}