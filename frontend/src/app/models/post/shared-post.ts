export interface SharedPost {
    postId: number;
    userId: number;
    email: string;
}