﻿using System.ComponentModel.DataAnnotations;

namespace Thread_.NET.Common.DTO.Post
{
    public class PostShareDTO
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Wrong Id")]
        public int PostId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Wrong Id")]
        public int UserId { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
