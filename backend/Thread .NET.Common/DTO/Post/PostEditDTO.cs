﻿using System.ComponentModel.DataAnnotations;

namespace Thread_.NET.Common.DTO.Post
{
    public class PostEditDTO
    {
        [Required]
        public int PostId { get; set; }
        [Required]
        public string PreviewImage { get; set; }
        [Required]
        public string Body { get; set; }
    }
}