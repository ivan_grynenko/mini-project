﻿using System.ComponentModel.DataAnnotations;

namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class EditCommentDTO
    {
        [Required]
        public int CommentId { get; set; }
        [Required]
        public string Body { get; set; }
    }
}
