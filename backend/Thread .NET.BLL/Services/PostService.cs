﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);            

            return createdPostDTO;
        }

        public async Task<Post> EditPost(PostEditDTO postDTO)
        {
            var post = await _context.Posts.FindAsync(postDTO.PostId);

            if (post == null)
                return null;

            if (post.PreviewId == null && postDTO.PreviewImage != null)
            {
                var image = new Image
                {
                    URL = postDTO.PreviewImage,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };
                await _context.Images.AddAsync(image);
                post.Preview = image;
            }
            else
            {
                var image = _context.Images.FirstOrDefault(e => e.Id == post.PreviewId);
                if (image != null && postDTO.PreviewImage == null)
                {
                    _context.Images.Remove(image);
                    post.PreviewId = null;
                }                  
                if (image != null && postDTO.PreviewImage != null && image.URL != postDTO.PreviewImage)
                    image.URL = postDTO.PreviewImage;
            }

            post.Body = postDTO.Body;
            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("EditPost", postDTO);

            return post;
        }

        public async Task<Post> DeletePost(int id)
        {
            var post = await _context.Posts.FindAsync(id);

            if (post == null)
                return null;

            var image = await _context.Images.FindAsync(post.PreviewId);
            var comments = _context.Comments.Where(e => e.PostId == post.Id);
            var reactions = _context.PostReactions.Where(e => e.PostId == post.Id);

            _context.Posts.Remove(post);

            if (comments.Count() > 0)
            {
                var commentsReacts = _context.CommentReactions.Where(e => comments.Select(c => c.Id).Contains(e.CommentId));

                if (commentsReacts.Count() > 0)
                    _context.CommentReactions.RemoveRange(commentsReacts);

                _context.Comments.RemoveRange(comments);
            }

            if (reactions.Count() > 0)
                _context.PostReactions.RemoveRange(reactions);

            if (image != null)
                _context.Images.Remove(image);

            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("DeletePost");

            return post;
        }
    }
}
