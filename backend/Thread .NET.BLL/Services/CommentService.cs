﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<CommentHub> _commentHub;

        public CommentService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> commentHub)
            : base(context, mapper)
        {
            _commentHub = commentHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            await _commentHub.Clients.All.SendAsync("NewComment");

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task<Comment> EditComment(EditCommentDTO newComment)
        {
            var comment = await _context.Comments.FindAsync(newComment.CommentId);

            if (comment == null)
                return null;

            comment.Body = newComment.Body;
            await _context.SaveChangesAsync();
            await _commentHub.Clients.All.SendAsync("EditComment");

            return comment;
        }

        public async Task<bool> DeleteComment(int id)
        {
            var comment = await _context.Comments.FindAsync(id);

            if (comment == null)
                return false;

            var reactions = _context.CommentReactions.Where(e => e.CommentId == id);

            if (reactions.Count() > 0)
                _context.CommentReactions.RemoveRange(reactions);

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
            await _commentHub.Clients.All.SendAsync("DeleteComment");

            return true;
        }
    }
}
