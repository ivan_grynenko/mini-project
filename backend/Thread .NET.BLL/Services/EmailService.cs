﻿using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.SignalR;
using Thread_.NET.BLL.Hubs;

namespace Thread_.NET.BLL.Services
{
    public class EmailService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private readonly IConfiguration _configuration;
        private readonly string hostEmail;
        private readonly string hostPassword;

        public EmailService(ThreadContext context, IMapper mapper, IConfiguration configuration, IHubContext<PostHub> postHub)
            : base(context, mapper)
        {
            _postHub = postHub;
            _configuration = configuration;
            hostEmail = _configuration["EmailCredentials:SenderEmail"];
            hostPassword = _configuration["EmailCredentials:SenderPassword"];
        }

        public async Task SharePost(PostShareDTO postShared)
        {
            var post = await _context.Posts.FindAsync(postShared.PostId);

            if (post == null)
                return;

            var sender = await _context.Users.FindAsync(postShared.UserId);
            var image = await _context.Images.FindAsync(post.PreviewId);

            var from = new MailAddress(hostEmail, "Support", Encoding.UTF8);
            var to = new MailAddress(postShared.Email);

            var client = GetSmtpClient();
            var message = new MailMessage(from, to)
            {
                Subject = $"Thread.NET: {sender?.UserName} shared a comment with you",
                SubjectEncoding = Encoding.UTF8,
                BodyEncoding = Encoding.UTF8,
                IsBodyHtml = true,
                Body = MainBodyBuilder(image?.URL, post.Body)
            };

            await client.SendMailAsync(message);

            client.Dispose();
            message.Dispose();

            await _postHub.Clients.All.SendAsync("PostShare");
        }

        public async Task LikeNotification(NewReactionDTO reaction)
        {
            var post = await _context.Posts.Where(e => e.Id == reaction.EntityId)
                .Join(_context.Users, p => p.AuthorId, u => u.Id, (p, u) => 
                new { p.Id, p.Body, Author = u.UserName, u.Email, p.PreviewId })
                .FirstOrDefaultAsync();

            if (post == null)
                return;

            var userWhoLiked = await _context.Users.FindAsync(reaction.UserId);
            var image = await _context.Images.FindAsync(post.PreviewId);

            var from = new MailAddress(hostEmail, "Support", Encoding.UTF8);
            var to = new MailAddress(post.Email);

            var client = GetSmtpClient();
            var message = new MailMessage(from, to)
            {
                Subject = "Thread.NET: You have a new reaction on your post",
                SubjectEncoding = Encoding.UTF8,
                BodyEncoding = Encoding.UTF8,
                IsBodyHtml = true,
                Body = BodyBuilderLike(userWhoLiked?.UserName, reaction.IsLike, image?.URL, post.Body)
            };

            await client.SendMailAsync(message);

            client.Dispose();
            message.Dispose();
        }

        private string BodyBuilderLike(string userName, bool liked, string imageUrl, string postBody)
        {
            var builder = new StringBuilder();
            builder.Append("<div><b>")
                .Append(userName != null ? userName : "User")
                .Append(liked ? " liked " : " disliked ")
                .Append("your post</div>")
                .Append(MainBodyBuilder(imageUrl, postBody));

            return builder.ToString();
        }

        private string MainBodyBuilder(string imageUrl, string postBody)
        {
            var builder = new StringBuilder();
            builder.Append("<div style=\"padding: 20px; max-width: 300px; text-align: left; background-color: powderblue;\">");

            if (!string.IsNullOrEmpty(imageUrl))
                builder.Append($"<p><img src=\"{imageUrl}\" style=\"position: relative; width: 100%;\"></p>");

            builder.Append($"<p>{postBody}</p></div>");

            return builder.ToString();
        }

        private SmtpClient GetSmtpClient()
        {
            return new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(hostEmail, hostPassword)
            };
        }
    }
}
