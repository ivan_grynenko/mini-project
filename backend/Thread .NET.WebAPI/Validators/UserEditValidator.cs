﻿using FluentValidation;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public class UserEditValidator : AbstractValidator<UserDTO>
    {
        public UserEditValidator()
        {
            RuleFor(u => u.UserName)
                .NotEmpty()
                .WithMessage("Username is mandatory.")
                .MinimumLength(3)
                .WithMessage("Username should be minimum 3 character.")
                .MaximumLength(50)
                .WithMessage("Username should be maximum 50 character.");

            RuleFor(u => u.Email)
                .EmailAddress();
        }
    }
}
