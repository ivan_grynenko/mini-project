﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly EmailService _emailService;

        public PostsController(PostService postService, LikeService likeService, EmailService emailService)
        {
            _postService = postService;
            _likeService = likeService;
            _emailService = emailService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPosts());
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            var result = await _likeService.LikePost(reaction);
            
            if (result)
                await _emailService.LikeNotification(reaction);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost([FromRoute] int id)
        {
            if (id < 1)
                return BadRequest();

            var result = await _postService.DeletePost(id);

            if (result == null)
                return NotFound();

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> EditPost([FromBody] PostEditDTO postDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var result = await _postService.EditPost(postDTO);

            if (result == null)
                return NotFound();

            return NoContent();
        }

        [HttpPost("share")]
        public async Task<IActionResult> SharePost([FromBody] PostShareDTO postShared)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var task = _emailService.SharePost(postShared);

            await task;

            if (task.IsCompletedSuccessfully)
                return NoContent();

            return StatusCode(500, "Error while sending the email");
        }
    }
}